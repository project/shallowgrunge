<?php
// $Id: 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language ?>" xml:lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">

<head>
  <title><?php print $head_title ?></title>
  <?php print $head ?>
  <?php print $styles ?>
  <?php print $scripts ?>
  <script type="text/javascript"><?php /* Needed to avoid Flash of Unstyle Content in IE */ ?> </script>
</head>

<body>

<div id="wrapper">
	<div id="header">
		<div id="logo">
            <?php if ($logo) { ?><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a><?php } ?>
			<?php if ($site_name) { ?><h1><a href="<?php print $front_page ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1><?php } ?>
			<p> <?php if ($site_slogan) { ?><?php print $site_slogan ?><?php } ?></p>
		</div>
		<div id="searchbox">
			<?php print $search_box ?>
		</div>
		<!-- end #search -->
	</div>
	<!-- end #header -->
	<div id="menu">
		<?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('class' => 'links', 'id' => 'navlist')) ?><?php } ?>
	</div>
	<!-- end #menu -->
	<div id="page">
		<div id="content">
			<div class="post">
				        <?php if ($mission) { ?><div id="mission"><?php print $mission ?></div><?php } ?>
        <?php print $breadcrumb ?>
        <h1 class="title"><?php print $title ?></h1>
        <div class="tabs"><?php print $tabs ?></div>
        <?php if ($show_messages) { print $messages; } ?>
        <?php print $help ?>
        <div class="entry"><?php print $content; ?></div>
        <?php print $feed_icons; ?>
			</div>
		</div>
		<!-- end #content -->
		<div id="sidebar">
			<?php if ($right) { ?> <?php print $right ?> <?php } ?>
		</div>
		<!-- end #sidebar -->
		<div style="clear: both;">&nbsp;</div>
	</div>
	<!-- end #page -->
</div>
	<div id="footer">
		<div class="message">
			<?php print $footer_message ?>
		</div>
		
		<div class="coded">Design by <a href="http://www.freecsstemplates.org/">Free CSS Templates</a>. Ported by <a href="http://goodwinsolutions.com">Goodwin Web Solutions</a>.
		</div>
		
	</div>
<!-- end #footer -->
</body>
</html>
